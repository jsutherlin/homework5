	.global _start
	
_start:

@Display your first and last name

	MOV R7, #4
	MOV R0, #1
	MOV R2, #16
	LDR R1, =string
	SWI 0


@Add the numbers 2 and 7

	MOV R3, #2
	ADD R3, R3, #7
	ADD R3, R3, #48
	
@Display the result

	LDR R4, =output
	STR R3, [R4]
	MOV R0, #1
	LDR R1, =output
	MOV R2, #5
	MOV R7, #4
	SWI 0
	



@Multiply the numbers 3 and 2

	MOV R3, #3
	MOV R2, #2
	MUL R5, R3, R2
	ADD R5, R5, #48
	
@Display the result
	
	LDR R4, =output
	STR R5, [R4]
	MOV R0, #1
	LDR R1, =output
	MOV R2, #5
	MOV R7, #4
	SWI 0
	
@Logically AND the numbers 42 and 7

	MOV R3, #42
	AND R3, R3, #7
	ADD R3, R3, #48
	
@Display the result

	LDR R4, =output
	STR R3, [R4]
	MOV R0, #1
	LDR R1, =output
	MOV R2, #5
	MOV R7, #4
	SWI 0
	
@Logically OR the numbers 5 and 2

	MOV R3, #5
	ORR R3, R3, #2
	ADD R3, R3, #48
	
@Display the result

	LDR R4, =output
	STR R3, [R4]
	MOV R0, #1
	LDR R1, =output
	MOV R2, #5
	MOV R7, #4
	SWI 0
	
@Perform the following mathematical expression:
@A = 24
@B = 13
@C = 3
@( A - B ) * C

	MOV R1, #24
	MOV R2, #13
	MOV R3, #3
	SUB R4, R1, R2
	MUL R0, R4, R3
	MOV R7, #1
	SWI 0 

_exit:

	MOV R7, #1
	SWI 0
	
@Save the result of this calculation in R0 and cleanly exit your program

.data
output:
.ascii "    \n"

.data
string:
.ascii "James Sutherlin\n"
